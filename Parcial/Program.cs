﻿    using System;

    namespace Parcial
    {
        class Program
        {
            static void Main(string[] args)
            {

                Console.WriteLine("Seleccione una de las siguientes opciones : ");
                Console.WriteLine("coloque su opcion de letra en mayuscula");

                Console.WriteLine("A : Ejercicio doblemente glorioso ");
                Console.WriteLine("B : Ejercicio formula uno ");
                Console.WriteLine("C : Ejercicio Liga de Leyendas ");
                Console.WriteLine("D : la escuela del chavo ");
                Console.WriteLine("//////////////////////////");

                String opcion = Console.ReadLine();


                switch (opcion)
                {
                    case "A":
                        Program.doblementeglorioso();
                    break;

                    case "B":
                        Program.formula_uno();
                        break;
                    case "C":
                        Program.liga_de_leyendas();
                        break;
                    case "D":
                        Program.el_chavo();
                        break;
                    default:

                        Console.WriteLine("No ejecuto ninguna opción adios");

                        Console.WriteLine("//////////////////////////");

                        break;
                    
                }
            }



            public static void doblementeglorioso() 
            {

                int t;
                t = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                int n;
                int a;
                int b;
                int acum;
                while ((t--) != 0)
                {
                    n = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                    acum = 0;
                    while ((n--) != 0)
                    {
                        a = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                        b = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                        if (a > b)
                        {
                            acum += 3;
                        }
                        else if (a == b)
                        {
                            acum += 1;
                        }
                        Console.Write(acum);
                        Console.Write("\n");
                    }
                }

                Console.ReadKey();

            }

            public static void liga_de_leyendas()
            {
                int T = int.Parse(Console.ReadLine());
                int[] ranking = new int[T];

                for (int i = 0; i < T; i++)
                {
                    ranking[i] = int.Parse(Console.ReadLine());
                }

                for (int i = 0; i < T; i++)
                {
                    int n = ranking[i];
                    if (n >= 1 && n <= 5)
                        Console.WriteLine("leyenda");
                    else if (n >= 6 && n <= 30)
                        Console.WriteLine("platino");
                    else if (n >= 31 && n <= 155)
                        Console.WriteLine("oro");
                    else if (n >= 156 && n <= 780)
                        Console.WriteLine("plata");
                    else if (n >= 781 && n <= 3905)
                        Console.WriteLine("bronce");
                }

                Console.ReadKey();
            }

            public static void formula_uno()
            {
                Console.WriteLine("Ingrese los casos de prueba:");

                int w, x, y, z;

                int T = int.Parse(Console.ReadLine()); //casos de prueba


                for (int i = 0; i < T; i++)
                {
                    w = int.Parse(ConsoleInput.ReadToWhiteSpace(true));

                    x = int.Parse(ConsoleInput.ReadToWhiteSpace(true));

                    y = int.Parse(ConsoleInput.ReadToWhiteSpace(true));

                    z = int.Parse(ConsoleInput.ReadToWhiteSpace(true));


                    double Vm2 = 0;

                    if (w < 21 && w > -21 && x < 101 && x >= 0 && y < 401 && y >= 0 && z < 41 && z > -21)
                    {
                        if (w == 0)
                        {
                            Vm2 = 0;
                        }
                        else
                        {
                            Vm2 = (((2 * w) * (6 * w) * (w)) / (4 * w));

                        }
                        double Vm = (13 * (Math.Pow(x, 2))) - (Math.Sqrt(49 * (Math.Pow(y, 2)))) + (3 * (Math.Pow(w, 2))) + (5 * x) + (8 * y) + (z) - (7 * (Math.Pow(x, 2))) - ((12 / 3) * x) - (6 * (Math.Pow(x, 2))) - (z) - Vm2;
                        Console.WriteLine("VELOCIDAD:");
                        Console.WriteLine(Vm);
                    }
                    else
                    {
                        Console.WriteLine("Los datos no son correctos");
                    }
                }
                Console.ReadKey();

            }

            public static void el_chavo() 
            {
        

            }


        }
    }

    internal static class ConsoleInput
    {
        private static bool goodLastRead = false;
        public static bool LastReadWasGood
        {
            get
            {
                return goodLastRead;
            }
        }

        public static string ReadToWhiteSpace(bool skipLeadingWhiteSpace)
        {
            string input = "";

            char nextChar;
            while (char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
            {
                //accumulate leading white space if skipLeadingWhiteSpace is false:
                if (!skipLeadingWhiteSpace)
                    input += nextChar;
            }
            //the first non white space character:
            input += nextChar;

            //accumulate characters until white space is reached:
            while (!char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
            {
                input += nextChar;
            }

            goodLastRead = input.Length > 0;
            return input;
        }

        public static string ScanfRead(string unwantedSequence = null, int maxFieldLength = -1)
        {
            string input = "";

            char nextChar;
            if (unwantedSequence != null)
            {
                nextChar = '\0';
                for (int charIndex = 0; charIndex < unwantedSequence.Length; charIndex++)
                {
                    if (char.IsWhiteSpace(unwantedSequence[charIndex]))
                    {
                        //ignore all subsequent white space:
                        while (char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
                        {
                        }
                    }
                    else
                    {
                        //ensure each character matches the expected character in the sequence:
                        nextChar = (char)System.Console.Read();
                        if (nextChar != unwantedSequence[charIndex])
                            return null;
                    }
                }

                input = nextChar.ToString();
                if (maxFieldLength == 1)
                    return input;
            }

            while (!char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
            {
                input += nextChar;
                if (maxFieldLength == input.Length)
                    return input;
            }

            return input;
        }
    }
